from people import Person
from functools import lru_cache


@lru_cache(maxsize=None)
def ancestors(id_):
    res = {id_: 0}
    for p in Person.all_[id_].parents:
        for k, v in ancestors(p).items():
            res[k] = v + 1
    return res


def get_family_relation(a, b):
    if a == b:
        return None
    aa = ancestors(a)
    ab = ancestors(b)
    matches = set(aa) & set(ab)
    try:
        best = min(matches, key=lambda k: aa[k] + ab[k])
    except ValueError:
        return None
    return (aa[best], ab[best])
