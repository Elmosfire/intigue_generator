from events import EventHandler, tqdm


class Clock:
    current_day = -1
    DAYS_IN_YEAR = 365


history = EventHandler('core')


@history.register
def next_day():
    Clock.current_day += 1


@history.register
def initialise():
    Clock.current_day += 1


@next_day.add_chain
def initialise_link():
    if Clock.current_day == 0:
        initialise()


def generate(tme):
    history.recreate_current_state()
    for _ in tqdm(range(tme * Clock.DAYS_IN_YEAR),desc='generating history'):
        next_day()
    history.save_status()


if __name__ == "__main__":
    generate(40)
