from pathlib import Path
import json
import inspect
from functools import partial
from tqdm import tqdm


def log_name(func):
    return f"{func.__module__}.{func.__name__}"

class EventHandler:
    def __init__(self, pt):
        self.pt = Path(pt)
        self.pt.mkdir(exist_ok=True)
        self.histfile = self.pt / "history.jsonv"
        self.linkfile = self.pt / "event_links.json"
        self.calls = {}
        self.current_chain = []
        self.debug_linkset = set()
        self.debug_triggerset = set()
        self.debug_htriggerset = set()
        self.debug_chainset = set()

    def register(self, func):
        trigger = Trigger(func, self)
        self.calls[log_name(func)] = trigger
        self.debug_triggerset.add(log_name(func))
        return trigger

    def register_untracked(self, func):
        trigger = Trigger(func, self)
        self.calls[log_name(func)] = trigger
        trigger.hidden = True
        self.debug_htriggerset.add(log_name(func))
        return trigger

    def store_event(self, data):
        with self.histfile.open('a') as file:
            try:
                file.write(json.dumps(data, indent=None) + '\n')
            except TypeError:
                print('data to write', data)
                raise

    def execute_event(self, data, chain=True):
        if self.current_chain:
            assert self.current_chain[-1] != 'invalid', "cannot call an event directly without using chaining"
            self.debug_linkset.add((self.current_chain[-1], data['type']))
        func = self.calls[data['type']]
        kwargs = data.copy()
        del kwargs['type']
        if chain:
            func.exec_chain(**kwargs)
        else:
            func.exec(**kwargs)

    def __call__(self, data):
        self.store_event(data)
        self.execute_event(data)

    def recreate_current_state(self):
        self.load_linkset()
        try:
            with self.histfile.open('r') as file:
                for line in tqdm(file, desc='replaying history'):
                    data = json.loads(line)
                    self.execute_event(data, chain=False)
        except FileNotFoundError:
            pass

        self.load_linkset()

    def save_linkset(self):
        with self.linkfile.open('w') as file:
            json.dump(dict(
                triggers=list(self.debug_triggerset),
                triggers_hidden=list(self.debug_htriggerset),
                chains=list(self.debug_chainset),
                links=list(f"{x}:{y}" for x, y in self.debug_linkset)
            ), file, indent=2)

    def save_status(self):
        self.save_linkset()

    def load_linkset(self):
        try:
            with self.linkfile.open('r') as file:
                data = json.load(file)
                self.debug_triggerset |= set(data['triggers'])
                self.debug_htriggerset |= set(data['triggers_hidden'])
                self.debug_chainset |= set(data['chains'])
                for x in data['links']:
                    self.debug_linkset.add(tuple(x.split(':')))
        except FileNotFoundError:
            pass


class Trigger:
    def __init__(self, trigger, parent):
        self.trigger = trigger
        self.chain = []
        self.export = []
        self.parent = parent
        self.hidden = False


    def exec(self, *args, **kwargs):
        self.trigger(*args, **kwargs)
        for x in self.export:
            x(*args, **kwargs)

    def __call__(self, *args, **kwargs):
        if self.hidden:
            self.parent.execute_event(self.event_json(*args, **kwargs))
        else:
            self.parent(self.event_json(*args, **kwargs))

    def exec_chain(self, *args, **kwargs):
        self.parent.current_chain.append('invalid')
        self.trigger(*args, **kwargs)
        self.parent.current_chain.pop()
        for x in self.export:
            x(*args, **kwargs)
        for x in self.chain:
            self.parent.current_chain.append(log_name(x))
            x(*args, **kwargs)
            self.parent.current_chain.pop()

    def add_chain(self, func):
        self.parent.debug_linkset.add((log_name(self.trigger), log_name(func)))
        self.parent.debug_chainset.add(log_name(func))
        self.chain.append(func)
        return func

    def add_chain_prior(self, func):
        self.parent.debug_linkset.add((log_name(self.trigger), log_name(func)))
        self.parent.debug_chainset.add(log_name(func))
        self.chain.insert(0,func)
        return func

    def add_export(self, func):
        self.export.append(func)
        return func

    def event_json(self, *args, **kwargs):
        res = kwargs.copy()
        argnames = inspect.getfullargspec(self.trigger)[0]
        res['type'] = log_name(self.trigger)
        for x, y in zip(argnames, args):
            res[x] = y

        return res


if __name__ == "__main__":
    e = EventHandler(Path('test.jsnv'))


    @e.register
    def test_main():
        print('test 1 done')


    @test_main.add_chain
    def trigger_test2():
        test2("from test 1")


    @e.register
    def test2(data):
        print('done 2', data)


    e.recreate_current_state()
    test_main()
