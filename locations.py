from core import Clock, history, initialise, next_day, generate
from random import randrange, choices, random
import people
from town_names import get_name

locations = {}


@history.register
def create_location(name):
    locations[name] = []


def get_home(id):
    return next(k for k, v in locations.items() if id in v)

@history.register
def build_village():
    pass

@initialise.add_chain_prior
def build_villages_init_link():
    for x in range(5):
        build_village()

@build_village.add_chain
def build_village_link():
    village_name = get_name()
    for i in range(15):
        create_location(f"{village_name}.house_{i}")


@history.register
def move_out(id):
    try:
        locations[get_home(id)].remove(id)
    except StopIteration:
        pass


@history.register
def move_to(id, newhome):
    locations[newhome].append(id)


def vacant():
    yield from [k for k, v in locations.items() if not v]


def move(id, new_location):
    move_out(id)
    move_to(id, new_location)


@history.register
def move_to_random_vacant(id):
    pass



@move_to_random_vacant.add_chain
def move_to_random_vacant_link(id):
    try:
        home = get_home(id)
    except StopIteration:
        home = ""
    village = home.split('.')[0]
    vacant_ = list(vacant())
    weights = [1 if x.startswith(village) else 0.1 for x in vacant_]

    newhome, = choices(vacant_, weights=weights, k=1)
    move(id, newhome)



@history.register
def move_in_with(a, b):
    pass

@move_in_with.add_chain
def move_in_with_link(a, b):
    move(a, get_home(b))

@people.person_on_init.add_chain
def move_person_to_random_home_on_init(id):
    move_to_random_vacant(id)

@people.death.add_chain
def move_out_on_death(id):
    move_out(id)

@people.become_adult.add_chain
def move_on_adult(id):
    move_to_random_vacant(id)

@people.set_parent.add_chain
def add_parent_move_in_link(child, parent):
    if people.Person.all_[parent].isFemale:
        move_in_with(child, parent)

