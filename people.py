from core import Clock, history, initialise, next_day, generate
from random import randrange, choice, random
import names


def roll(c):
    return random() < c


MAX_AGE = 80
ADULT_HOOD = 18


class Person:
    all_ = {}
    counter = 0

    def __init__(self, id):
        self.id = id
        self.all_[self.id] = self
        self.birth_day = Clock.current_day
        self.isFemale = True
        self.name = ''
        self.lastname = ''
        self.parents = []
        self.children = []
        self.orientation = (True,False)
        self.alive = True
        self.last_child = 0

    def age(self):
        return (Clock.current_day - self.birth_day) / Clock.DAYS_IN_YEAR

    @classmethod
    def all_alive(cls):
        for k, v in cls.all_.items():
            if v.alive:
                yield k


@history.register
def birth(id, name, gender, orientation):
    person = Person(id)
    person.name = name
    person.isFemale = gender
    person.orientation = orientation
    Person.counter += 1


def birth_wrapper():
    gender = bool(randrange(2))
    name = names.get_first_name(["male", 'female'][gender])
    if roll(0.93):
        orientation = (gender, not gender)
    else:
        orientation = choice([(False, False), (not gender, gender), (True, True)])
    birth(Person.counter, name, bool(gender), orientation)


@history.register
def set_last_name(id, lastname):
    Person.all_[id].lastname = lastname


@history.register
def death(id):
    Person.all_[id].alive = False


@history.register
def procreate(a, b):
    pass


def procreate_wrapper(a, b):
    if Person.all_[a].last_child + 220 > Clock.current_day:
        return
    if Person.all_[b].last_child + 220 > Clock.current_day:
        return
    if len(Person.all_[a].children) >= 3:
        return
    if len(Person.all_[b].children) >= 3:
        return
    if not Person.all_[a].isFemale != Person.all_[b].isFemale:
        return
    procreate(a, b)


@history.register
def set_parent(child, parent):
    Person.all_[child].parents.append(parent)
    Person.all_[parent].children.append(child)
    Person.all_[parent].last_child = Clock.current_day


@history.register
def meet_up(a, b):
    pass


@history.register
def person_day_start(id):
    pass


@history.register
def person_on_init(id):
    pass

@history.register
def become_adult(id):
    pass



'''
@meet_up.add_chain
def meetup_procreate_link(a, b):
    person_a = Person.all_[a]
    person_b = Person.all_[b]
    if person_a.age() > 20 and person_b.age() > 20 and person_a.isFemale == 0 and person_b.isFemale == 1:
        if roll(0.0001):
            procreate(a, b)
'''


@procreate.add_chain
def procreate_link(a, b):
    id = Person.counter
    birth_wrapper()
    set_parent(id, a)
    set_parent(id, b)
    lastname = Person.all_[a].lastname
    set_last_name(id, lastname)


@next_day.add_chain
def start_of_day_link():
    for x in list(Person.all_alive()):
        person_day_start(x)


@person_day_start.add_chain
def become_adult_link(id):
    if Clock.current_day - Person.all_[id].birth_day == Clock.DAYS_IN_YEAR * ADULT_HOOD:
        become_adult(id)




'''
@person_day_start.add_chain
def start_of_day_meetup_link(id):
    for y in list(Person.all_alive()):
        if roll(0.001):
            meet_up(id, y)
'''

@person_day_start.add_chain
def start_of_day_die_link(id):
    if roll(0.00005) or Person.all_[id].age() > MAX_AGE:
        death(id)


@initialise.add_chain
def initialise_people():
    for id in range(50):
        birth_wrapper()
        lastname = names.get_last_name()
        set_last_name(id, lastname)
        person_on_init(id)



if __name__ == "__main__":
    generate(40)
