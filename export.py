import people
import family
import relations
import locations
import gatherings
from core import history, Clock
import yaml
from cleanup_relationships import cleanup as cleanup_relationships
from types import SimpleNamespace

alldata = {}

with open('defs.yml') as file:
    DEFS = SimpleNamespace(**yaml.load(file))


def get_orientation(gender, orientation):
    name = f'o{int(orientation[not gender])}_{int(orientation[gender])}'
    if '/' in DEFS.orientation[name]:
        return DEFS.orientation[name].split('/')[gender]
    else:
        return DEFS.orientation[name]


@people.birth.add_export
def export_birth_data(id, name, gender, orientation):
    alldata[id] = {}
    alldata[id]['first_name'] = name
    alldata[id]['gender'] = gender
    alldata[id]['orientation'] = get_orientation(gender, orientation)
    alldata[id]['birth_day'] = Clock.current_day
    alldata[id]['alive'] = True
    alldata[id]['relations'] = {}
    alldata[id]['locations'] = dict(current=None, history=[])


@people.set_last_name.add_export
def export_lastname(id, lastname):
    alldata[id]['last_name'] = lastname


@people.death.add_export
def export_death_data(id):
    alldata[id]['death_day'] = Clock.current_day
    alldata[id]['alive'] = False


@relations.set_friendship.add_export
def export_friendship(a, b, modifier):
    if b not in alldata[a]['relations']:
        alldata[a]['relations'][b] = []
    if a not in alldata[b]['relations']:
        alldata[b]['relations'][a] = []
    alldata[a]['relations'][b].append(dict(time=Clock.current_day, type=f'friend_{modifier}'))
    alldata[b]['relations'][a].append(dict(time=Clock.current_day, type=f'friend_{modifier}'))


@relations.set_romantic.add_export
def export_romantic(a, b, modifier):
    if b not in alldata[a]['relations']:
        alldata[a]['relations'][b] = []
    if a not in alldata[b]['relations']:
        alldata[b]['relations'][a] = []
    alldata[a]['relations'][b].append(dict(time=Clock.current_day, type=f'romantic_{modifier}'))
    alldata[b]['relations'][a].append(dict(time=Clock.current_day, type=f'romantic_{modifier}'))


@locations.move_to.add_export
def export_location(id, newhome):
    alldata[id]['locations']['history'].append(dict(time=Clock.current_day, location=newhome))
    alldata[id]['locations']['current'] = newhome


@locations.move_out.add_export
def export_location(id):
    alldata[id]['locations']['current'] = None


def cleanup_ages():
    for x in alldata:
        if not alldata[x]['alive']:
            alldata[x]['age'] = (alldata[x]['death_day'] - alldata[x]['birth_day']) // Clock.DAYS_IN_YEAR
        else:
            alldata[x]['age'] = (Clock.current_day - alldata[x]['birth_day']) // Clock.DAYS_IN_YEAR


def export_family_relations():
    for a in alldata:
        for b in alldata:
            if b not in alldata[a]['relations']:
                alldata[a]['relations'][b] = []
            try:
                x, y = family.get_family_relation(a, b)
            except TypeError:
                pass
            else:
                if x <= 2 and y <= 2:
                    alldata[a]['relations'][b].append(dict(time=None, type=f'family_{x}_{y}'))


if __name__ == "__main__":
    history.recreate_current_state()
    export_family_relations()
    cleanup_ages()
    cleanup_relationships(alldata)
    yaml.dump(alldata, open("core/people.yml", 'w'), sort_keys=False)
