from core import Clock, history, initialise, next_day, generate
from random import randrange, choices, random, choice
import people
import family
import locations
import relations


@next_day.add_chain
def gather_link():
    if people.roll(0.1):
        loc_invite = choice(list(locations.locations))

        if not locations.locations[loc_invite]:
            return

        targets = [k for k, v in locations.locations.items() if v]
        targets.remove(loc_invite)

        weights = [relations.location_weight_con(loc_invite, x) for x in targets]

        sz = choice([0, 1, 2, 3, 5, 7, 10])

        if sz:
            others = list(set(choices(targets, weights, k=sz)))
        else:
            others = []

        locs = [loc_invite] + others

        gather_by_locations(locs)


@history.register
def gather_by_locations(locs):
    pass


@gather_by_locations.add_chain
def gather_loc_people_link(locs):
    people_ = []
    for x in locs:
        people_.extend(locations.locations[x])

    gather_by_people(people_)


@history.register
def gather_by_people(people_):
    pass


@gather_by_people.add_chain
def gather_by_people_link(people_):
    for x in people_:
        attended_party(x)
        for y in people_:
            if x != y:
                if people.roll(0.1):
                    people.meet_up(x, y)


@history.register
def attended_party(id):
    pass
