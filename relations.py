from core import Clock, history, initialise, next_day, generate
from random import randrange, choice, random
import people
import family
import locations

friendships = {}
romantic = {}

FRIENDSHIP_INCREASE_CHANCE = 0.1
FRIENDSHIP_DECREASE_CHANCE = 0.02

ROMANTIC_INCREASE_CHANCE = 0.1
ROMANTIC_DECREASE_CHANCE = 0.02


def get_family_relation(a, b):
    x = family.get_family_relation(a, b)
    if x is None:
        return 0
    else:
        return max(0, 5 - sum(x))


def get_friendship(a, b):
    return friendships.get((a, b), 0)


def get_romantic(a, b):
    return romantic.get((a, b), 0)


def is_single(a):
    return not any(get_romantic(a, b) > 0 for b in people.Person.all_alive())


def get_romantic_partner(a):
    return next(b for b in people.Person.all_alive() if get_romantic(a, b) > 0)


def opinion(a, b):
    return get_friendship(a, b) + 3 * get_family_relation(a, b) + 10 * get_romantic(a, b)


@history.register
def set_friendship(a, b, modifier):
    friendships[(a, b)] = modifier
    friendships[(b, a)] = modifier


@history.register
def set_romantic(a, b, modifier):
    romantic[(a, b)] = modifier
    romantic[(b, a)] = modifier


@people.meet_up.add_chain
def reevalute_relationship(a, b):
    if a == b:
        return
    friendship_diff = 0

    if people.roll(FRIENDSHIP_DECREASE_CHANCE):
        friendship_diff -= 1
    if people.roll(FRIENDSHIP_INCREASE_CHANCE):
        friendship_diff += 1
    if friendship_diff:
        current_friendship = get_friendship(a, b)
        new_friendship = current_friendship + friendship_diff
        if current_friendship != new_friendship and 0 <= new_friendship <= 4:
            set_friendship(a, b, new_friendship)

    romantic_diff = 0

    if people.roll(ROMANTIC_INCREASE_CHANCE):
        romantic_diff += 1
    if people.roll(ROMANTIC_DECREASE_CHANCE):
        romantic_diff -= 1

    if romantic_diff:
        current_romantic = get_romantic(a, b)
        if current_romantic == 0:
            if romantic_diff < 0:
                viable = 0
            else:
                age_a = people.Person.all_[a].age()
                age_b = people.Person.all_[b].age()
                gender_a = people.Person.all_[a].isFemale
                gender_b = people.Person.all_[b].isFemale
                compatible_a = people.Person.all_[a].orientation[gender_b]
                compatible_b = people.Person.all_[b].orientation[gender_a]
                viable = is_single(a) and is_single(b) and \
                         age_a > age_b / 2 + 7 and age_b > age_a / 2 + 7 and \
                         get_family_relation(a, b) == 0 and \
                         compatible_a and compatible_b
        else:
            viable = 1

        if viable:
            new_romantic = current_romantic + romantic_diff
            if romantic_diff < 0:
                new_romantic = 0
            if current_romantic != new_romantic and 0 <= new_romantic <= 3:
                set_romantic(a, b, new_romantic)


@people.person_day_start.add_chain
def start_of_day_procreate_link(id):
    if people.roll(0.001):
        if not is_single(id):
            other = get_romantic_partner(id)
            people.procreate_wrapper(id, other)


@set_romantic.add_chain
def set_romantic_move_link(a, b, modifier):
    if modifier == 2:
        locations.move_in_with(a, b)


def location_weight_con(loc1, loc2):
    return max(opinion(x, y) for x in locations.locations[loc1] for y in locations.locations[loc2]) \
           + 1 + 10 * (loc1.split('.')[0] == loc2.split('.')[0])
