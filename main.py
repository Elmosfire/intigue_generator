import people
import family
import relations
import locations
import gatherings
from core import history, Clock, generate
from graphviz import Digraph




if __name__ == "__main__":
    generate(40)
    dot = Digraph(comment='Event_stack')
    for x in history.debug_triggerset:
        dot.node(x, shape='parallelogram', color='red', fontcolor='red')
    for x in history.debug_htriggerset:
        dot.node(x, shape='box', color='green', fontcolor='green')
    for x in history.debug_chainset:
        dot.node(x, shape='oval', color='blue', fontcolor='blue')
    for x,y in history.debug_linkset:
        dot.edge(x, y)

    dot.render('core/trigger_stack.gv', view=True)

