import yaml

with open('defs.yml') as file:
    RELATIONSHIPS = yaml.load(file)['relationships']

relationship_value = {k: i for i, k in enumerate(RELATIONSHIPS)}

def get_relationship(name,right_gender):
    if '/' in RELATIONSHIPS[name]:
        return RELATIONSHIPS[name].split('/')[right_gender]
    else:
        return RELATIONSHIPS[name]


def cleanup(alldata):
    for left in alldata:
        sorted_rels = []
        for right in alldata:
            all_ = alldata[left]['relations'][right]
            all_types = [x['type'] for x in all_]
            if not all_types:
                continue
            best_relationship = max(all_types, key=relationship_value.get)
            value = relationship_value[best_relationship]
            sorted_rels.append((-value, right, best_relationship))
        sorted_rels.sort()
        alldata[left]['relations_clean'] = {}
        for _, right, name in sorted_rels:
            alldata[left]['relations_clean'][right] = {'relation': get_relationship(name, alldata[right]['gender']), 'history':[]}
            for x in alldata[left]['relations'][right]:
                alldata[left]['relations_clean'][right]['history'].append(
                    dict(
                        time=x['time'],
                        relation=get_relationship(x['type'], alldata[right]['gender'])
                    )
                )
                alldata[left]['relations_clean'][right]['history'].sort(key=lambda x: x['time'] if x['time'] is not None else -float('inf'))
        del alldata[left]['relations']

