from flask import Flask, render_template, request
import yaml

app = Flask(__name__)

DAYS_IN_YEAR = 365


def ts_to_str(ts):
    return f'year {ts // DAYS_IN_YEAR}, day {ts % DAYS_IN_YEAR}'


def td_to_str(ts):
    return f'{ts // DAYS_IN_YEAR} years and {ts % DAYS_IN_YEAR} days'


with open('../core/people.yml') as file:
    alldata = yaml.load(file)


@app.route('/')
def hello():
    return render_template('index.html', people=list(alldata))


@app.route('/insert/person/short/')
def insert_person_short():
    person = int(request.args.get('id'))
    print(person)

    data = alldata[person]
    data['link'] = f"/person/?id={person}"

    return render_template('person_short.html', **data)


@app.route('/insert/timestamp/')
def insert_timestamp():
    try:
        ts = int(request.args.get('ts', 0))
    except ValueError:
        return "invalid time"

    return ts_to_str(ts)

@app.route('/insert/timedelta/')
def insert_timedelta():
    td = int(request.args.get('td', 0))

    return td_to_str(td)


@app.route('/person/')
def person():
    person = int(request.args.get('id'))

    data = alldata[person].copy()

    data['id'] = person



    return render_template('person.html', **data)

@app.route('/insert/relationship/')
def insert_relationship():
    person = int(request.args.get('id'))
    aux = request.args.get('aux', None)



    if aux is not None:

        history = alldata[person]['relations_clean'][int(aux)]['history']
        relation = alldata[person]['relations_clean'][int(aux)]['relation']
        history = [tuple(x.values()) for x in history]

        return render_template('relation_insert.html', aux=int(aux), history=history, id=person, relation=relation)

    else:

        return render_template('relation_insert_full.html', id=person, all=list(alldata[person]['relations_clean']))

@app.route('/insert/location/')
def insert_location():
    person = int(request.args.get('id'))

    history = alldata[person]['locations']['history']
    current = alldata[person]['locations']['current']
    history = [tuple(x.values()) for x in history]

    return render_template('locations_insert.html', history=history, id=person, current=current)



app.run()
